const functions = require('firebase-functions');
const axios = require('axios');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.sendArkeselSMS = functions.https.onRequest((request, response) => {
    const { api_key, to, sms, from } = request.query;
    const url = `https://sms.arkesel.com/sms/api?action=send-sms&api_key=${api_key}&to=${to}&from=${from}&sms=${sms}`;

    axios.get(url)
        .then(res => {
            console.log(res.data);
            response.send(res.data);
        })
        .catch(error => {
            console.log(error);
            response.send(error);
        });
});
